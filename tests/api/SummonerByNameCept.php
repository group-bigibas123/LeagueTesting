<?php
require("_bootstrap.php");
$I = new ApiTester($scenario);
$I->wantTo('get a summoners info using his summonername');
$I->haveHttpHeader('Content-Type', 'application/json');
foreach ($summonerArray as $name => $id) {
    $I->sendGET("/v1.4/summoner/by-name/${name}?api_key=${api_key}");
    $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
    $I->seeResponseIsJson();
    $I->seeResponseContains($name);
    $I->seeResponseMatchesJsonType([
        strtolower($name) => [
            'id' => 'integer:>0',
            'name' => "string",
            'profileIconId' => 'integer:>0',
            'revisionDate' => 'integer:>0',
            'summonerLevel' => 'integer:<31'
        ]]);

    $I->seeResponseContainsJson([
        strtolower($name) => [
            'name' => $name,
            'id' => $id
        ]
    ]);
}